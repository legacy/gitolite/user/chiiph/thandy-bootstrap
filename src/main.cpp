/* Copyright (c) 2011, The Tor Project, Inc. */
/* See LICENSE for licensing information */

#include <QtGui>

#include "ThandyWrapper.h"
#include "UpdaterDialog.h"

#if defined(Q_WS_WIN32)
# include <windows.h>
#endif

void usage()
{
  QString usage("");

  qDebug() << "Valid arguments::";
  qDebug() << "                   --wait 5";
  qDebug() << "                   --no-download";
  qDebug() << "                   --check";
  qDebug() << "                   --datadir /path/to/datadir/";
  qDebug() << "                   --restartcmd \"app arg1 arg2 ...\"";
}

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  bool wait, nextwait, check, download, error, hasdatadir, restart;
  wait = nextwait = hasdatadir = error = check = false;
  download = true;
  int sec = 0;
  QString datadir = ".", restartCmd = "";

  QStringList args = app.arguments();

  args.removeAt(0);

  foreach(QString arg, args) {
    if(nextwait) {
      nextwait = false;
      sec = arg.toInt();
      continue;
    }

    if(hasdatadir) {
      hasdatadir = false;
      datadir = arg;
      continue;
    }

    if(restart) {
      restart = false;
      restartCmd = arg;
      continue;
    }

    if(arg == "--wait") {
      wait = true;
      nextwait = true;
    } else if(arg == "--no-download") {
      download = false;
    } else if(arg == "--check") {
      check = true;
    } else if(arg == "--datadir") {
      hasdatadir = true;
    } else if(arg == "--restartcmd") {
      restart = true;
    } else
      error = true;
  }

  if(error) {
    usage();
    return 0;
  }

  ThandyWrapper *thandy = new ThandyWrapper(datadir, check);
  UpdaterDialog *dialog;

  if(wait)
#if defined(Q_WS_WIN32)
    Sleep(sec*1000);
#else
    sleep(sec);
#endif

  if(check) {
    thandy->check(download);
    QObject::connect(thandy, SIGNAL(done()), thandy, SLOT(deleteLater()));
    QObject::connect(thandy, SIGNAL(done()), QCoreApplication::instance(), SLOT(quit()));
  } else {
    dialog = new UpdaterDialog(thandy, restartCmd);
    dialog->show();
  }

  return app.exec();
}

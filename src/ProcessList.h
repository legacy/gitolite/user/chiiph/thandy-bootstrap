/* Copyright (c) 2011, The Tor Project, Inc. */
/* See LICENSE for licensing information */

#ifndef PROCESSLIST_H
#define PROCESSLIST_H

#include <QtCore>

#if defined(Q_WS_WIN)
#  include <windows.h>
#else
#  include <QProcess>
#endif

class ProcessList
{
 public:
  /** Default constructor */
  ProcessList();
  /** Destructor */
  ~ProcessList();

  /** Multiplatform wrapper for *_get() */
  const QHash<quint64, QString> get();

 private:
  /** Returns a Hash for the current running processes in the given
      platform */
#if defined(Q_WS_X11)
  const QHash<quint64, QString> x11_get();
#elif defined(Q_WS_WIN32)
  const QHash<quint64, QString> win32_get();
#elif defined(Q_WS_MAC)
  const QHash<quint64, QString> mac_get();
#endif
};

#endif
